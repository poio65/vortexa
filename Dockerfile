FROM python:3.9
ADD cargo_movements.py .
ADD cargo_movements.csv .
ADD requirements.txt .
RUN python -m pip install -r requirements.txt
CMD ["python", "./cargo_movements.py", "-f", "./cargo_movements.csv"]
