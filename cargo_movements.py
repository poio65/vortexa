#! /usr/bin/env python
# -*- coding: utf-8 -*-
""" Cargo Movements processing
cd vortexa
docker build -t vortexa-test .
docker run vortexa-test
"""

import numpy as np
import pandas as pd
import argparse


class CargoMovements:
    """
    Uses a Panda's dataframe to return lists of cargo movements. Could
    implement a specific CargoMovement class to return list of individual
    object, however, the performance would be greatly affected by this
    approach because of the potential size of the Dataframe.
    JSON wrappers are provided to ensure compatibility with other
    applications and a potential RESTful API.
    """

    def __init__(self, datasource):
        """
        CargoMovements Constructor
        Receives a Panda Dataframe as input
        Separates malformed entries (incomplete data in row) and calculates
        durations of all movements
        :param datasource:
        """
        if type(datasource) is not pd.core.frame.DataFrame:
            raise ValueError('Invalid dataset format', datasource)
        if datasource.empty:
            raise ValueError('Empty dataset')
        self.df = datasource
        # Separate incomplete entries in a secondary dataframe
        self.incomplete = self.df[self.df.isnull().any(axis=1)]
        # ... and clean the dataframe
        self.df.dropna(inplace=True)
        self.calculate_durations()

    def calculate_durations(self):
        """
        Calculate the duration of cargo movements for correct entries
        Uses map and lambda to optimise calculations
        :return:
        """
        # Use map + lambda to transform string dates to pandas timestamps while
        # optimising column operations (mark missing dates as pd.NaT)
        start_timestamps = self.df['start_timestamp']\
            .map(lambda x: pd.to_datetime(x) if pd.notna(x) else pd.NaT)
        end_timestamps = self.df['end_timestamp']\
            .map(lambda x: pd.to_datetime(x) if pd.notna(x) else pd.NaT)
        # Calculate time deltas
        self.df['duration'] = end_timestamps - start_timestamps
        # Save time deltas in seconds and in tuples for output formatting
        self.df['duration_in_seconds'] = self.df['duration']\
            .map(lambda x: x.total_seconds() if pd.notna(x) else np.NaN)
        self.df['duration_components'] = self.df['duration']\
            .map(lambda x: {'days': x.components.days,
                            'hours': x.components.hours,
                            'minutes': x.components.minutes,
                            'seconds': x.components.seconds
                            } if pd.notna(x) else np.NaN)

    def get_durations_by_vessel_class(self, vessel_class, json=False, tabular=False):
        """
        Return the list of cargo movements of a certain vessel class
        :param vessel_class: name of vessel class (might be partial name
        :param json: Set to True if you want results in json format
        :param tabular: Set to True if you want text tabular output
        :return: a Panda's dataframe containing the filtered vessels movements
        """
        df = self.df
        df = df[df['vessel_class'].str.contains(vessel_class, na=False)]
        return CargoMovements.format_dataframe(df, json, tabular)

    def get_incomplete(self, json=False, tabular=False):
        """
        Get the list of malformed entries (incomplete information)
        :param json: Set to True if you want results in json format
        :param tabular: Set to True if you want text tabular output
        :return: a Panda's dataframe containing the filtered vessels movements
        """
        return CargoMovements.format_dataframe(self.incomplete, json, tabular)

    def get_all(self, json=False, tabular=False):
        """
        Dump full list of valid cargo movements
        :param json: Set to True if you want results in json format
        :param tabular: Set to True if you want text tabular output
        :return: a Panda's dataframe containing the filtered vessels movements
        """
        return CargoMovements.format_dataframe(self.df, json, tabular)

    def get_by_load_port(self, port, json=False, tabular=False):
        """
        Retrieve list of valid cargo movements by source port
        :param port: [partial] name of departing port
        :param json: Set to True if you want results in json format
        :param tabular: Set to True if you want text tabular output
        :return: a Panda's dataframe containing the filtered vessels movements
        """
        df = self.df
        df = df[df['load_port'].str.contains(port, na=False)]
        return CargoMovements.format_dataframe(df, json, tabular)

    def get_by_discharge_port(self, port, json=False, tabular=False):
        """
        Retrieve list of valid cargo movements by destination port
        :param port: [partial] name of target port
        :param json: Set to True if you want results in json format
        :param tabular: Set to True if you want text tabular output
        :return: a Panda's dataframe containing the filtered vessels movements
        """
        df = self.df
        df = df[df['discharge_port'].str.contains(port, na=False)]
        return CargoMovements.format_dataframe(df, json, tabular)

    def get_by_departure_date(self, load_date, load_port=None, json=False, tabular=False):
        """
        Retrieve list of valid cargo movements by departure date
        :param load_date: date of departure (pandas format)
        :param load_port: [partial] name of departing port
        :param json: Set to True if you want results in json format
        :param tabular: Set to True if you want text tabular output
        :return: a Panda's dataframe containing the filtered vessels movements
        """
        df = self.df
        df = df[df['start_timestamp'].str.contains(load_date, na=False)]
        if load_port:
            df = df[df['load_port'].str.contains(load_port, na=False)]
        return CargoMovements.format_dataframe(df, json, tabular)

    def get_by_arrival_date(self, discharge_date, discharge_port=None, json=False, tabular=False):
        """
        Retrieve list of valid cargo movements by arrival date
        :param discharge_date: date of arrival (pandas format)
        :param discharge_port: [partial] name of target port
        :param json: Set to True if you want results in json format
        :param tabular: Set to True if you want text tabular output
        :return: a Panda's dataframe containing the filtered vessels movements
        """
        df = self.df
        df = df[df['end_timestamp'].str.contains(discharge_date, na=False)]
        if discharge_port:
            df = df[df['discharge_port'].str.contains(discharge_port, na=False)]
        return CargoMovements.format_dataframe(df, json, tabular)

    def fetch(self, queries=None, json=False, tabular=False):
        """
        This method can replace all previous query methods, however, the queries
        must be specified as a list of tuples containing the key and the value
        [('load_port', 'Singapore'), ('discharge_port', 'Chittagong')]
        These conditions will be applied sequentially filtering the list of cargo
        movements accordingly
        :param queries: set of tuples containing
        :param json: Set to True if you want results in json format
        :param tabular: Set to True if you want text tabular output
        :return: a Panda's dataframe containing the filtered vessels movements
        """
        df = self.df
        for (k, v) in queries:
            df = df[df[k].str.contains(v, na=False)]
            if df.empty:
                break
        return CargoMovements.format_dataframe(df, json, tabular)

    # TODO: Create a CMUtil class to include support methods
    @staticmethod
    def format_dataframe(df, json=False, tabular=False):
        """
        Formats the dataframe according in json, tabular or plain
        :param df: dataframe to format
        :param json: format as a json string
        :param tabular: format as a table
        :return: a string version of the dataframe
        """
        if json:
            return CargoMovements.to_json(df)
        if tabular:
            return df.to_markdown()
        return df

    @staticmethod
    def to_json(df):
        """
        Returns a dataframe in json format
        :param df: Pandas dataframe
        :return: corresponding json string
        """
        # return individual documents in case dataset is large
        return df.to_json(orient='records')[1:-1].replace('},{', '}\n{')


if __name__ == '__main__':

    parser = argparse.ArgumentParser('''
     Loads/cleans a cargo movements datafile and calculates the duration 
     python cargo_movements.py -f datafilenmae
     ''')
    parser.add_argument("-f", "--filename", dest="fname", required=True, help="Cargo movements datafile name")

    args = parser.parse_args()

    try:
        dataset = pd.read_csv(args.fname)
        cm = CargoMovements(dataset)
        print('\nTEST JSON\n',
              cm.get_all(json=True))
        print('\nALL CARGO MOVEMENTS IN TABLE FORMAT\n',
              cm.get_all(tabular=True))
        print('\nINCOMPLETE ENTRIES\n',
              cm.get_incomplete(tabular=True))
        print('\nCARGO MOVEMENTS BY VESSEL CLASS\n',
              cm.get_durations_by_vessel_class(vessel_class='tiny_tanker', tabular=True))
        print('\nCARGO MOVEMENTS BY DEPARTURE PORT\n', cm.get_by_load_port('Singapore', tabular=True))
        print('\nCARGO MOVEMENTS BY ARRIVAL PORT\n',
              cm.get_by_discharge_port('Cao', tabular=True))
        print('\nCARGO MOVEMENTS BY DEPARTURE PORT AND DATE\n',
              cm.get_by_departure_date('2016-12-22', load_port='Singapore', tabular=True))
        print('\nCARGO MOVEMENTS BY ARRIVAL PORT AND DATE\n',
              cm.get_by_arrival_date('2017-01-01', discharge_port='Singapore', tabular=True))
        print('\nCARGO MOVEMENTS BY ARRIVAL PORT AND DATE (EXPERT)\n',
              cm.fetch(queries=[('end_timestamp', '2017-01-01'), ('discharge_port', 'Sing')], tabular=True))
    except FileNotFoundError as e:
        print('Input file: ' + args.fname + ' not found')
    except Exception as e:
        print('Error while processing cargo movements', e)
